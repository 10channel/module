OBJS = main.o 
OBJ_NAME = main
INDEX = -lm -lSDL2 -lSDL2_gfx -L/usr/lib
CC = gcc

all: $(OBJ_NAME)

$(OBJ_NAME):$(OBJS)
	gcc $(OBJS) $(INDEX) -o $(OBJ_NAME)

.c.o:
	$(CC) -c $<

.PHONY:clean

clean:
	rm -f *.o core $(OBJ_NAME)
