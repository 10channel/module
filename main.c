/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief メインプログラム
 * @version 0.1
 * @date 2021-09-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */

//インクルード
#include <SDL2/SDL.h>                // SDLを用いるために必要なヘッダファイル
#include <SDL2/SDL2_gfxPrimitives.h> // 描画関係のヘッダファイル
#include <stdio.h>
#include <stdlib.h>
//ここまで

//定義
#define WINDOW_SIZE_X 1280 //画面サイズを指定
#define WINDOW_SIZE_Y 720

typedef enum {
    GS_PLAYING = 1,
    GS_END     = 2,
    GS_START   = 3,
} GAMESTATUS;

typedef struct _GAMEDirector {
    int stts;
    SDL_Point mouse_pos;
} GAMEDirector;
//ここまで

//関数のプロトタイプ宣言

int func_keyboard(void* args);

/**
 * @brief メイン関数
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
//グローバル変数
GAMEDirector gdirector;
int buttonflag = 0; //押し続けている間は１になる
SDL_Renderer* renderer;

int main(int argc, char* argv[])
{
    SDL_Window* window;

    SDL_Thread* keyboard_thread;
    SDL_mutex* mtx = SDL_CreateMutex(); //相互排除を用いる

    gdirector.stts = GS_START;

    //SDL初期化
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
        printf("failed to initialize SDL.\n");
        exit(EXIT_FAILURE);
    }

    if ((window = SDL_CreateWindow("Multi-Thread Mario", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_SIZE_X, WINDOW_SIZE_Y, 0)) == NULL) {
        printf("failed to create window.\n");
        exit(-1);
    }
    // レンダリングコンテキスト（RC）作成
    if ((renderer = SDL_CreateRenderer(window, -1, 0)) == NULL) {
        printf("failed to create renderer.\n");
        exit(EXIT_FAILURE);
    }
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"); // 拡大縮小を滑らかにする（線形フィルタで）

    keyboard_thread = SDL_CreateThread(func_keyboard, "keyboard_theard", mtx);
    gdirector.stts  = GS_PLAYING;

    //描画処理
    SDL_SetRenderDrawColor(renderer, 225, 225, 225, 225);
    SDL_RenderClear(renderer);
    while (gdirector.stts != GS_END) {

        if (buttonflag == 1) {
            filledCircleColor(renderer, gdirector.mouse_pos.x, gdirector.mouse_pos.y, 10, 0xff0000ff);
        }
        SDL_RenderPresent(renderer);
    }

    //終了処理
    SDL_WaitThread(keyboard_thread, NULL);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_DestroyMutex(mtx);
    SDL_Quit();

    printf("終了\n");
    return 0;
}

int func_keyboard(void* args)
{
    SDL_Event event;

    while (gdirector.stts != GS_END) {
        SDL_mutex* mtx = (SDL_mutex*)args;
        if (SDL_PollEvent(&event)) {
            SDL_LockMutex(mtx);
            switch (event.type) {
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    gdirector.stts = GS_END;
                    break;
                case SDLK_c:
                    SDL_SetRenderDrawColor(renderer, 225, 225, 225, 225);
                    SDL_RenderClear(renderer);
                    break;

                default:
                    break;
                }
                break;
            case SDL_QUIT:
                gdirector.stts = GS_END;
                break;

            case SDL_MOUSEBUTTONDOWN:
                switch (event.button.button) {
                case SDL_BUTTON_LEFT:
                    buttonflag = 1;
                    break;
                }
                break;
            case SDL_MOUSEBUTTONUP:
                switch (event.button.button) {
                case SDL_BUTTON_LEFT:
                    buttonflag = 0;
                    break;
                }
                break;
            case SDL_MOUSEMOTION:
                //マウスの位置を取得
                gdirector.mouse_pos.x = event.motion.x;
                gdirector.mouse_pos.y = event.motion.y;
                break;
            default:
                break;
            }
            SDL_UnlockMutex(mtx);
        }
    }
    return 0;
}